import request from '@/utils/request'

// 查询栏目管理列表
export function listCategory(query) {
  return request({
    url: '/cms/category/list',
    method: 'get',
    params: query
  })
}

// 查询全部栏目数据集合
export function listAllCategory() {
  return request({
    url: '/cms/category/allList',
    method: 'get'
  })
}

// 查询栏目管理详细
export function getCategory(categoryId) {
  return request({
    url: '/cms/category/' + categoryId,
    method: 'get'
  })
}

//
export function categoryTreeselect(query) {
  return request({
    url: '/cms/category/treeselect',
    method: 'get',
    params: query
  })
}
// 新增栏目管理
export function addCategory(data) {
  return request({
    url: '/cms/category',
    method: 'post',
    data: data
  })
}

// 修改栏目管理
export function updateCategory(data) {
  return request({
    url: '/cms/category',
    method: 'put',
    data: data
  })
}

// 删除栏目管理
export function delCategory(categoryId) {
  return request({
    url: '/cms/category/' + categoryId,
    method: 'delete'
  })
}
