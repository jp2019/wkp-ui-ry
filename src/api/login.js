import request from '@/utils/request'

// 账号密码登录方法
export function login(username, password, code, uuid) {
  return request({
    url: '/auth/login',
    method: 'post',
    data: { username, password, code, uuid }
  })
}

// 短信登录方法
export function smsLogin(phonenumber, smsCode, uuid) {
  const data = {
    phonenumber,
    smsCode,
    uuid
  }
  return request({
    url: '/auth/smsLogin',
    method: 'post',
    data: data
  })
}

// 刷新方法
export function refreshToken() {
  return request({
    url: '/auth/refresh',
    method: 'post'
  })
}

// 获取用户详细信息
export function getInfo() {
  return request({
    url: '/system/user/getInfo',
    method: 'get'
  })
}

// 退出方法
export function logout() {
  return request({
    url: '/auth/logout',
    method: 'delete'
  })
}

// 获取验证码
export function getCodeImg() {
  return request({
    url: '/code',
    method: 'get'
  })
}

// 发送短信验证码
export function getSmsCode(phonenumber) {
  return request({
    url: '/smsCode/'+phonenumber,
    method: 'get'
  })
}
