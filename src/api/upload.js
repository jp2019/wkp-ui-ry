import request from '@/utils/request'

//上传图片
export function imageUpload(data) {
  return request({
    url: '/file/upload',
    method: 'post',
    data: data
  })
}

// 上传视频
export function videoUpload(data) {
  return request({
    url: '/file/upload',
    method: 'post',
    data: data
  })
}
