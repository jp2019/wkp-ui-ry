import request from '@/utils/request'

// 查询站点设置列表
export function listSite(query) {
  return request({
    url: '/cms/site/list',
    method: 'get',
    params: query
  })
}

// 查询全部站点数据集合
export function listAllSite() {
  return request({
    url: '/cms/site/allList',
    method: 'get'
  })
}

// 查询站点设置详细
export function getSite(siteId) {
  return request({
    url: '/cms/site/' + siteId,
    method: 'get'
  })
}

// 新增站点设置
export function addSite(data) {
  return request({
    url: '/cms/site',
    method: 'post',
    data: data
  })
}

// 修改站点设置
export function updateSite(data) {
  return request({
    url: '/cms/site',
    method: 'put',
    data: data
  })
}

// 删除站点设置
export function delSite(siteId) {
  return request({
    url: '/cms/site/' + siteId,
    method: 'delete'
  })
}
