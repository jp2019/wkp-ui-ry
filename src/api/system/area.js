import request from '@/utils/request'

// 查询行政区划列表
export function listArea(query) {
  return request({
    url: '/system/area/list',
    method: 'get',
    params: query
  })
}
