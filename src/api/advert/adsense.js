import request from '@/utils/request'

// 查询广告位列表
export function listAdsense(query) {
  return request({
    url: '/advert/adsense/list',
    method: 'get',
    params: query
  })
}

// 查询全部广告位数据集合
export function listAllAdsense() {
  return request({
    url: '/advert/adsense/allList',
    method: 'get'
  })
}

// 查询广告位详细
export function getAdsense(adsenseId) {
  return request({
    url: '/advert/adsense/' + adsenseId,
    method: 'get'
  })
}

// 新增广告位
export function addAdsense(data) {
  return request({
    url: '/advert/adsense',
    method: 'post',
    data: data
  })
}

// 修改广告位
export function updateAdsense(data) {
  return request({
    url: '/advert/adsense',
    method: 'put',
    data: data
  })
}

// 删除广告位
export function delAdsense(adsenseId) {
  return request({
    url: '/advert/adsense/' + adsenseId,
    method: 'delete'
  })
}
